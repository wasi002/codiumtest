//
//  User.swift
//  CodiumTest
//
//  Created by Muhammad Waseem on 30/08/2019.
//  Copyright © 2019 Muhammad Waseem. All rights reserved.
//

import Foundation


struct User : Codable {
    let id : String?
    let name : String?
    let age : Int?
}
