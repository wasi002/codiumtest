//
//  UserListViewController.swift
//  CodiumTest
//
//  Created by Muhammad Waseem on 30/08/2019.
//  Copyright © 2019 Muhammad Waseem. All rights reserved.
//

import UIKit

class UserListViewController: UIViewController {

    @IBOutlet private weak var userTableView: UITableView!

    var users = [User]()

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func addContact(_ sender: Any) {
        
        self.performSegue(withIdentifier: "AddContactSegue", sender: true)

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "AddContactSegue" {
            let servicesVC = segue.destination as! AddUserViewController
            servicesVC.delegate = self
            
        }
    }
    
}

extension UserListViewController: AddUserProtocol {
    func contactAdded(user: User) {
        self.users.append(user)
        self.userTableView.reloadData()
    }
    

}


extension UserListViewController:UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier:NSString = "UserCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier as String, for: indexPath) as! UserCell

        let user = users[indexPath.row]
   
        cell.userTitle.text = "\(user.name ??  "") , \(user.age  ??  0)"

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    
    }
    
    
}
