//
//  AddUserViewController.swift
//  CodiumTest
//
//  Created by Muhammad Waseem on 30/08/2019.
//  Copyright © 2019 Muhammad Waseem. All rights reserved.
//

import UIKit


protocol AddUserProtocol: class {
    func contactAdded(user: User)
    
}

class AddUserViewController: UIViewController {

    @IBOutlet weak var idTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var ageTextField: UITextField!
    
    
    weak var delegate: AddUserProtocol?

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func addUser(_ sender: Any) {
        self.view.endEditing(true)
        if idTextField.text?.count == 0 || nameTextField.text?.count == 0 || ageTextField.text?.count == 0 {
            let alert = UIAlertController(title: "Alert", message: "Please enter All Information.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            return
        }
        
        let user = User(id: idTextField?.text, name: nameTextField?.text, age: Int(ageTextField?.text ?? "0"))
        
        delegate?.contactAdded(user: user)
        
        self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
}
