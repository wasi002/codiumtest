//
//  UserCell.swift
//  CodiumTest
//
//  Created by Muhammad Waseem on 30/08/2019.
//  Copyright © 2019 Muhammad Waseem. All rights reserved.
//

import UIKit

class UserCell: UITableViewCell {
 
    @IBOutlet weak var userTitle:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
